#include <iostream>
int add(int a, int b) {
    if (a == 8200 || b == 8200 || a + b == 8200)
    {
        throw(std::string("error"));
    }
    return a + b;
}

int  multiply(int a, int b) {
  int sum = 0;
  for(int i = 0; i < b; i++) {
    sum = add(sum, a);
  };
  if (a == 8200 || b == 8200 || sum == 8200)
  {
      throw(std::string("error"));
  }
  return sum;
}

int  pow(int a, int b) {
  int exponent = 1;
  for(int i = 0; i < b; i++) {
    exponent = multiply(exponent, a);
  };
  if (a == 8200 || b == 8200 || exponent == 8200)
  {
      throw(std::string("error"));
  }
  return exponent;
}

int main(void) {
    std::cout << "pow" << std::endl;
    try
    {
        std::cout << pow(5, 5) << std::endl;
        std::cout << pow(8200, 5) << std::endl;
    }
    catch (const std::string& errorString)
    {
        std::cerr<<"This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year" << std::endl;
    }

    std::cout << std::endl;
    std::cout << "multiply" << std::endl;

    try
    {
        std::cout << multiply(5, 5) << std::endl;
        std::cout << multiply(820, 10) << std::endl;
    }
    catch (const std::string& errorString)
    {
        std::cerr << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year" << std::endl;
    }

    std::cout << std::endl;
    std::cout << "add" << std::endl;

    try
    {
        std::cout << add(5, 5) << std::endl;
        std::cout << add(8200, 0) << std::endl;
    }
    catch (const std::string& errorString)
    {
        std::cerr << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year" << std::endl;
    }
}