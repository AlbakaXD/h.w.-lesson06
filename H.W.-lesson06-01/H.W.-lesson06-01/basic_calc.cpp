#include <iostream>
#define ERROR_CODE -1

int add(int a, int b) {
    int flag = 0;
    if (a == 8200 || b == 8200 || (a + b) == 8200)
    { 
        flag = ERROR_CODE;
    }
    if (flag == ERROR_CODE)
    {
        return -1;
    }
  return a + b;
}

int  multiply(int a, int b) {
  int sum = 0;
  int flag = 0;
  for(int i = 0; i < b; i++) {
    sum = add(sum, a);
  };
  if (a == 8200 || b == 8200 || sum == 8200)
  {
      flag = ERROR_CODE;
  }
  if (flag == ERROR_CODE)
  {
      return -1;
  }
  return sum;
}

int  pow(int a, int b) {
  int exponent = 1;
  int flag = 0;
  for(int i = 0; i < b; i++) {
    exponent = multiply(exponent, a);
  };
  if (a == 8200 || b == 8200 || exponent == 8200)
  {
      flag = ERROR_CODE;
  }
  if (flag == ERROR_CODE)
  {
      return -1;
  }
  return exponent;
}

bool check(int num)
{
    if (num < 0)
    {
        std::cerr << "This user is not authorized to access 8200, please enterdifferent numbers, or try to get clearance in 1 year" << std::endl;
        return false;
    }
    return true;
}

int main(void) {
    std::cout << "pow" << std::endl;
    if (check(pow(5, 5)))
    {
        std::cout << pow(5, 5) << std::endl;
    }
    if (check(pow(8200, 5)))
    {
        std::cout << pow(8200, 5) << std::endl;
    }
    std::cout << std::endl;

    std::cout << "multiply" << std::endl;
    if (check(multiply(5, 5)))
    {
        std::cout << multiply(5, 5) << std::endl;
    }
    if (check(multiply(820, 10)))
    {
        std::cout << multiply(820, 10) << std::endl;
    }
    std::cout << std::endl;

    std::cout << "add" << std::endl;
    if (check(add(5, 5)))
    {
        std::cout << add(5, 5) << std::endl;
    }
    if (check(add(8200, 0)))
    {
        std::cout << add(8200, 0) << std::endl;
    }
}