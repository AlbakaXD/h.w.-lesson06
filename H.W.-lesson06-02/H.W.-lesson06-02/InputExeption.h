#pragma once
#include <exception>

class InputExeption : public std::exception
{
	virtual const char* what() const
	{
		return "This is an input exception!\n";
	}
};