#pragma once
#include "shape.h"
#include "MathUtils.h"
#include "shapeexception.h"
#include <iostream>
class Hexagon : public Shape
{
public:
    void draw();
    Hexagon(std::string name, std::string color, double side);
    void setSide(double a);
    double getArea();

private:
    double side;

};