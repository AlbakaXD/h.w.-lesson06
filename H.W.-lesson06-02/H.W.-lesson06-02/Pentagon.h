#pragma once
#include "shape.h"
#include "MathUtils.h"
#include "shapeexception.h"
#include <iostream>
class Pentagon : public Shape
 {
public:
    void draw();
    Pentagon(std::string name, std::string color, double side);
    void setSide(double a);
    double getArea();

private:
    double side;

};