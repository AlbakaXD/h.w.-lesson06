#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "shapeException.h"
#include "InputExeption.h"
#include "Pentagon.h"
#include "Hexagon.h"

int main()
{
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180; int height = 0, width = 0, side = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	Pentagon penta(nam, col, side);
	Hexagon hex(nam, col, side);

	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;
	Shape* ptrpanta = &penta;
	Shape* ptrhex = &hex;


	
	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p', pentagon = 'e', hexagon = 'h'; char shapetypeC; std::string shapetype;
	char x = 'y';
	while (x != 'x') {
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, e = pentagon, h = hexagon" << std::endl;
		std::cin >> shapetype;
		while (shapetype.length() > 1)
		{
			std::cerr << "Warning - Don't try to build more than one shape at once" << std::endl;
			std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, e = pentagon, h = hexagon" << std::endl;
			std::cin >> shapetype;
		}
		shapetypeC = shapetype[0];
		try
		{

			switch (shapetypeC) {
			case 'c':
				std::cout << "enter color, name,  rad for circle" << std::endl;
				std::cin >> col >> nam >> rad;
				if (std::cin.fail())
				{
					std::cin.clear();
					std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
					throw InputExeption();
				}
				else
				{
					circ.setColor(col);
					circ.setName(nam);
					circ.setRad(rad);
					ptrcirc->draw();
				}
				break;
			case 'q':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				if (std::cin.fail())
				{
					std::cin.clear();
					std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
					throw InputExeption();
				}
				else
				{
					quad.setName(nam);
					quad.setColor(col);
					quad.setHeight(height);
					quad.setWidth(width);
					ptrquad->draw();
				}
				break;
			case 'r':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				if (std::cin.fail())
				{
					std::cin.clear();
					std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
					throw InputExeption();
				}
				else
				{
					rec.setName(nam);
					rec.setColor(col);
					rec.setHeight(height);
					rec.setWidth(width);
					ptrrec->draw();
				}
				break;
			case 'p':
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				std::cin >> nam >> col >> height >> width >> ang >> ang2;
				if (std::cin.fail())
				{
					std::cin.clear();
					std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
					throw InputExeption();
				}
				else if ((ang < 0 || ang > 180) || (ang2 < 0 || ang2 > 180))
				{
					throw shapeException();
				}
				else
				{
					para.setName(nam);
					para.setColor(col);
					para.setHeight(height);
					para.setWidth(width);
					para.setAngle(ang, ang2);
					ptrpara->draw();
				}
				break;
			case 'e':
				std::cout << "enter name, color, side" << std::endl;
				std::cin >> nam >> col >> side;
				if (std::cin.fail())
				{
					std::cin.clear();
					std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
					throw InputExeption();
				}
				else
				{
					penta.setName(nam);
					penta.setColor(col);
					penta.setSide(side);
					ptrpanta->draw();
				}
				break;
			case 'h':
				std::cout << "enter name, color, side" << std::endl;
				std::cin >> nam >> col >> side;
				if (std::cin.fail())
				{
					std::cin.clear();
					std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
					throw InputExeption();
				}
				else
				{
					hex.setName(nam);
					hex.setColor(col);
					hex.setSide(side);
					ptrhex->draw();
				}
				break;
			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			std::cin.get() >> x;
		}
		catch (std::exception& e)
		{			
			printf(e.what());
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}



		system("pause");
		return 0;
	
}