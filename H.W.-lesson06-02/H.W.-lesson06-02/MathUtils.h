#pragma once
class MathUtils 
{
public:
	static double CalPentagonArea(double side);
	static double CalHexagonArea(double side);
};
