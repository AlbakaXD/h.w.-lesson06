#include "Pentagon.h"

void Pentagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "Side is " << this->side << std::endl << "Area is " << getArea() << std::endl;
}

Pentagon::Pentagon(std::string a, std::string b, double side) : Shape(a, b)
{
	if (side < 0)
	{
		throw shapeException();
	}
	else
	{
		setSide(side);
	}
}

void Pentagon::setSide(double a)
{
	if (a < 0)
	{
		throw shapeException();
	}
	this->side = a;
}

double Pentagon::getArea()
{
	return MathUtils::CalPentagonArea(this->side);
}
